import java.util.HashMap;
import java.util.Map;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.StackPaneBuilder;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends Application {

    private static final int UNITSIZE = 9;
    private static final int WIDTH = UNITSIZE;
    private static final int HEIGHT = UNITSIZE;

    private static final int BOARD_WIDTH = 1500;
    private static final int BOARD_HEIGHT = 900;

    private static int generationCounter = 0;
    private static boolean useColor = false;

    private Map<String, StackPane> boardMap = new HashMap<>();
    private Board board = new Board(BOARD_WIDTH / UNITSIZE, BOARD_HEIGHT / UNITSIZE);
    private ContainerPanel containerPanel = new ContainerPanel();


    final Timeline timeline = new Timeline(
            new KeyFrame(Duration.ZERO, new EventHandler() {
                @Override
                public void handle(Event event) {
                    iterateBoard();
                }
            }), new KeyFrame(Duration.millis(100)));

    BorderPane root = new BorderPane();
    Pane gameOfLifePanel = new Pane();

    @Override
    public void start(Stage primaryStage) {
        timeline.setCycleCount(Timeline.INDEFINITE);

        Scene scene = new Scene(root, BOARD_WIDTH, BOARD_HEIGHT);
        scene.getStylesheets().add("gol.css");

        timeline.play();

        // Create a board with dead cells
        for (int x = 0; x < BOARD_WIDTH; x = x + WIDTH) {
            for (int y = 0; y < BOARD_HEIGHT; y = y + HEIGHT) {
                StackPane cell = StackPaneBuilder.create().layoutX(x).layoutY(y).prefHeight(HEIGHT).prefWidth(WIDTH).styleClass("dead-cell").build();
                gameOfLifePanel.getChildren().add(cell);

                //Store the cell in a HashMap for fast access
                //in the iterateBoard method.
                boardMap.put(x + " " + y, cell);
            }
        }

        containerPanel.getButtonStart().addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandlerStart);
        containerPanel.getButtonStop().addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandlerStop);
        containerPanel.getButtonRandom().addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandlerRandom);
        containerPanel.getButtonLys().addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandlerLys);
        containerPanel.getSliderSpeed().addEventHandler(MouseEvent.MOUSE_RELEASED, eventHandlerSpeed);
        containerPanel.getButtonClear().addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandlerClear);

        root.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandlerClick);
        root.setTop(containerPanel);
        root.setBottom(gameOfLifePanel);

        primaryStage.setTitle("Game of Life");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    EventHandler<MouseEvent> eventHandlerStart = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            timeline.play();
        }
    };

    EventHandler<MouseEvent> eventHandlerStop = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            timeline.stop();
        }
    };

    EventHandler<MouseEvent> eventHandlerRandom = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            String text = containerPanel.getTextField().getText();
            Double density = Double.parseDouble(text);
            board.initBoard(density);
        }
    };

    EventHandler<MouseEvent> eventHandlerLys = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            useColor = !useColor;
        }
    };

    EventHandler<MouseEvent> eventHandlerClear = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            board.clearBoard();
        }
    };

    EventHandler<MouseEvent> eventHandlerClick = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {

            String type = containerPanel.getComboBox().getValue().toString();
            int x = (int) e.getSceneX() / UNITSIZE;
            int y = (int) e.getSceneY() / UNITSIZE;
            System.out.println(type + ": " + x + ", " + y);

            switch (type) {
                case "Glider":
                    board.createGlider(x, y);
                    break;
                case "Boat":
                    board.createBoat(x, y);
                    break;
                case "Spaceship":
                    board.createSpaceship(x, y);
                    break;
                case "Glidergun":
                    board.createGlidergun(x, y);
                    break;
                case "Column":
                    board.createColumn(x, y);
                    break;
                case "Infinite":
                    board.createInfinite(x, y);
                    break;
                case "Pulsar":
                    board.createPulsar(x, y);
                    break;
                case "Toad":
                    board.createToad(x, y);
                    break;
                case "Block":
                    board.createBlock(x, y);
                    break;
                case "Single cell":
                    board.setPosition(x, y - 2, 1);
                    break;
                case "Traffic":
                    board.createTraffic(x, y);
                    break;
                case "Lightspeed":
                    board.createlightspeeed(x, y);
                    break;
                case "Sparky":
                    board.createSparky(x, y);
                    break;
                case "Star":
                    board.createStar(x, y);
                    break;
            }

        }
    };

    EventHandler<MouseEvent> eventHandlerSpeed = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            timeline.getKeyFrames().set(1, new KeyFrame(Duration.millis(containerPanel.getSliderSpeed().getValue() * 10)));
            timeline.stop();
            timeline.play();
        }
    };

    private void iterateBoard() {
        board.nextPopulation();
        containerPanel.getAliveLabel().setText("Celler i live: " + String.valueOf(board.getAliveCounter()));
        boolean hasYoungGen = false;

        for (int x = 0; x < board.getWidth(); x++) {
            for (int y = 0; y < board.getHeight(); y++) {
                StackPane pane = boardMap.get(x * WIDTH + " " + y * HEIGHT);
                pane.getStyleClass().clear();
                // If the cell at (x,y) is a alive use css styling 'alive-cell'
                // otherwise use the styling 'dead-cell'.
                int cellGen = board.getField(x, y);

                if (board.getField(x, y) >= 1) {
                    if (useColor) {
                        pane.getStyleClass().add("alive-cell-color-" + cellGen);
                    } else {
                        pane.getStyleClass().add("alive-cell-" + cellGen);
                    }
                } else {
                    pane.getStyleClass().add("dead-cell");
                }

                if(cellGen < 10 && cellGen != 0) {
                    hasYoungGen = true;
                }
            }
        }

        if(board.getAliveCounter() != 0 && hasYoungGen) {
            generationCounter++;
        }
        containerPanel.getGenerationLabel().setText("Generation: " + generationCounter);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
