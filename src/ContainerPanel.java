import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

/**
 * Created by oliver on 17-11-2017.
 */
public class ContainerPanel extends Pane {

    private Button buttonStart;
    private Button buttonStop;
    private ToggleButton buttonLys;
    private Label labelSpeed;
    private Slider sliderSpeed;
    private Button buttonRandom;
    private TextField textField;
    private Label aliveLabel;
    private Button buttonClear;
    private Label generationLabel;
    private final ComboBox comboBox;

    public ContainerPanel() {

        buttonStart = new Button("PLAY");
        buttonStop = new Button("STOP");
        buttonLys = new ToggleButton("Use Color");
        labelSpeed = new Label("SPEED");
        sliderSpeed = new Slider();
        buttonRandom = new Button("Random");
        textField = new TextField();
        aliveLabel = new Label("Celler i live: ");
        buttonClear = new Button("Clear board");
        generationLabel = new Label("Generation: ");
        comboBox = new ComboBox(getComboBoxOptions());

        setContainerPanel();
    }

    public void setContainerPanel(){
        sliderSpeed.setMin(10);
        sliderSpeed.setMax(100);
        sliderSpeed.setValue(50);
        sliderSpeed.setShowTickLabels(true);
        sliderSpeed.setShowTickMarks(true);

        sliderSpeed.setMajorTickUnit(50);
        sliderSpeed.setMinorTickCount(5);
        sliderSpeed.setBlockIncrement(10);

        buttonStart.setLayoutX(10);
        buttonStop.setLayoutX(60);

        buttonLys.setLayoutX(900);

        buttonRandom.setLayoutX(1100);
        buttonRandom.setMaxWidth(80);
        textField.setLayoutX(1180);
        textField.setMaxWidth(50);
        textField.setText("0.5");

        buttonClear.setLayoutX(1400);

        aliveLabel.setLayoutX(230);
        generationLabel.setLayoutX(120);

        comboBox.setValue("Single cell");
        comboBox.setLayoutX(350);

        labelSpeed.setLayoutX(650);
        labelSpeed.setMaxWidth(50);
        sliderSpeed.setLayoutX(700);
        sliderSpeed.setMaxWidth(200);

        this.getChildren().add(buttonStart);
        this.getChildren().add(buttonStop);
        this.getChildren().add(labelSpeed);
        this.getChildren().add(buttonRandom);
        this.getChildren().add(buttonLys);
        this.getChildren().add(sliderSpeed);
        this.getChildren().add(comboBox);
        this.getChildren().add(textField);
        this.getChildren().add(buttonClear);
        this.getChildren().add(aliveLabel);
        this.getChildren().add(generationLabel);
    }

    public ComboBox getComboBox() {
        return comboBox;
    }

    public Button getButtonStart() {
        return buttonStart;
    }

    public Button getButtonStop() {
        return buttonStop;
    }

    public ToggleButton getButtonLys() {
        return buttonLys;
    }

    public Label getLabelSpeed() {
        return labelSpeed;
    }

    public Slider getSliderSpeed() {
        return sliderSpeed;
    }

    public Button getButtonRandom() {
        return buttonRandom;
    }

    public TextField getTextField() {
        return textField;
    }

    public Label getAliveLabel() {
        return aliveLabel;
    }

    public Button getButtonClear() {
        return buttonClear;
    }

    public Label getGenerationLabel() {
        return generationLabel;
    }

    public ObservableList<String> getComboBoxOptions() {
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Single cell",
                        "Block",
                        "Glider",
                        "Boat",
                        "Spaceship",
                        "Glidergun",
                        "Column",
                        "Infinite",
                        "Pulsar",
                        "Toad",
                        "Traffic",
                        "Lightspeed",
                        "Sparky",
                        "Star"
                );
        return options;
    }

}
